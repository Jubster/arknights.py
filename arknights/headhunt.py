import datetime
import argparse
import calendar
import sys

# Add all used arguments
parser = argparse.ArgumentParser(
    description="Prints out the amount of pulls you will have on the given date"+ \
    "\nAssumes in calculation that you have done your dailies and weeklies "+ \
    "including annihilation",
    formatter_class=argparse.RawTextHelpFormatter
)
parser.add_argument(
    "ORIGINIUM",
    type=int,
    help="Amount of originium you are willing to use for the banner",
)
parser.add_argument(
    "ORUNDUM",
    type=int,
    help="Amount of orundum you have",
)
parser.add_argument(
    "TICKETS",
    type=int,
    help="Amount of free headhunt tickets you have",
)
parser.add_argument(
    "MONTHLY",
    help="Set state of monthly ticket (ex.: True/False, 1/0])",
)
parser.add_argument(
    "DATE",
    help="Date for the banner becoming available, formatted as YY/MM/DD \
    (ex.: 22/01/01, 22/12/31)\n\
    Defaults to current date, allowing you to assess your current situation",
    nargs="?",
    default=datetime.datetime.now().strftime("%y/%m/%d"),
)
args = parser.parse_args()

try:
    args.MONTHLY=bool(int(args.MONTHLY))
except:
    try:
        args.MONTHLY= True if str(args.MONTHLY).lower()=="true" else False
    except:
        print(f"Couldn't get correct typing from MONTHLY: {args.MONTHLY}")
        sys.exit()

# amount of orundum gained by time delta
weekly = 2300
daily = 300 if bool(args.MONTHLY) else 100

# Counts mondays between dates. Used for counting how many times
# weekly rewards should be counted
def weekday_count(start, end):
  mondays = 0
  seventeenths = 0 # Daily login rewards give you a ticket every 17th
  for i in range((end - start).days):
    date = (start + datetime.timedelta(days=i+1)).strftime("%d")
    day = (start + datetime.timedelta(days=i+1)).weekday()
    mondays = mondays + 1 if day==0 else mondays + 0
    seventeenths = seventeenths + 1 if date == "17" else seventeenths + 0
  return mondays,seventeenths

# Establishes the dates to be used and converts them to datetime objects
# Exits on input type error
try:
    todayDate = [year, month, day] = [
        int(x) for x in str(datetime.datetime.now().strftime("%y/%m/%d")).split("/")
    ]
    bannerDate = [year, month, day] = [
        int(x) for x in str(args.DATE).split("/")
    ]
except ValueError:
    print(f'Error in date input: {args.DATE}')
    sys.exit()
else:
    todayDate = datetime.datetime(todayDate[0],todayDate[1],todayDate[2])
    bannerDate = datetime.datetime(bannerDate[0],bannerDate[1],bannerDate[2])
    # Can't look ahead into the past. That's a time paradox
    if bannerDate < todayDate: bannerDate = todayDate

# Calculate the amount of pulls currently
headhunts_now=int((args.ORIGINIUM*180+args.ORUNDUM)/600)+args.TICKETS
# Calculate the amount of pulls gained by the banner date
headhunts_banner=int(
    (
        weekday_count(todayDate,bannerDate)[0]*weekly + \
        (bannerDate-todayDate).days*daily + \
        weekday_count(todayDate,bannerDate)[1]*600
    ) \
    /600
)

print(f"You will have {headhunts_now+headhunts_banner} pulls")
