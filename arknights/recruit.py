import argparse
import sys
from itertools import combinations

from lists import recruitables

parser = argparse.ArgumentParser()
parser.add_argument(
    "-l",
    "--list",
    action="store_true",
    dest="listMode",
    default=False,
    help="Enables list mode, allowing you list every tag for the given operator(s)",
)
parser.add_argument(
    "-s",
    "--show",
    action="store_true",
    dest="showMode",
    default=False,
    help="Also show all possible results",
)
parser.add_argument(
    "tags",
    help="Recruit tags or operators when in list mode",
    nargs="+",
    type=str
)
args = parser.parse_args()

if len(args.tags) > 5 and not args.listMode:
    print("Error: too many tags")
    sys.exit()


def capitalize(argument):
    """Handle operators that need unique handling for proper formatting"""
    edgeCases = [
        argument == "dps",
        argument == "dp-recovery",
        argument == "aoe",
        argument == "chen",
        argument == "silverash",
        argument == "thermal-ex",
        argument == "feater",
    ]
    if edgeCases[0] or edgeCases[1]:
        return argument[0:4].upper() + argument[4:]
    elif edgeCases[2]:
        return argument[0].upper() + argument[1] + argument[2].upper()
    elif edgeCases[3]:
        return argument[0:2].title() + "'" + argument[2:]
    elif edgeCases[4]:
        return argument[0:6].title() + argument[6:].title()
    elif edgeCases[5]:
        return argument[0:8].title() + argument[8:].upper()
    elif edgeCases[6]:
        return argument[0:2].upper() + argument[2:]
    else:
        return argument.replace("_", " ").title()


tags = [capitalize(i) for i in args.tags]


def color(operator):
    """Colorize operator names in output"""
    rarity = int(recruitables[operator][0])
    if rarity == 6:
        return "\033[0;91m" + operator + "\033[0m"
    elif rarity == 5:
        return "\033[0;33m" + operator + "\033[0m"
    elif rarity == 4:
        return "\033[0;35m" + operator + "\033[0m"
    elif rarity == 3:
        return "\033[0;94m" + operator + "\033[0m"
    elif rarity == 2:
        return "\033[0;92m" + operator + "\033[0m"
    else:
        return operator


# Allows checking for tags of an operator
# If I ever get an eureka moment, I'll also make it list any guarantee tags for the operator
# Not gonna spend any brain power on it for now though
if args.listMode:
    for operator in tags:
        if operator not in recruitables:
            print("\nError: [", operator, "] was not found", sep="")
        else:
            print("\n", color(operator), "Has following tags")
            print(" - ".join(recruitables[operator][1::]))
    sys.exit()

matches = {}
comboMatches = {}
for operator in recruitables:
    # This is pretty disgusting, but eh. Just filters out anything below the highest tier guarantee tag. So if you get Top Operator, even if you have Senior Operator, the print out will only show 6 stars.
    if (
        ("Top Operator" in tags and recruitables[operator][0] == "6")
        or (
            "Senior Operator" in tags
            and "Top Operator" not in tags
            and recruitables[operator][0] == "5"
        )
        or (
            "Top Operator" not in tags
            and "Senior Operator" not in tags
            and recruitables[operator][0] < "6"
        )
    ):
        for tag in tags:
            if tag in recruitables[operator]:
                matches.setdefault(operator, [recruitables[operator][0]]).append(tag)

if "Top Operator" in tags:
    rating = "6"
elif "Senior Operator" in tags:
    rating = "5"
else:
    rating = "4+"

# Searches for any 4+ guarantees and puts them into sets with the key being the tag combo
for i in range(1, int(len(tags)) + 1):
    for combo in combinations(tags, i):
        for operator in matches:
            if all(x in matches[operator][1::] for x in combo):
                if matches[operator][0] >= "4":
                    comboMatches.setdefault(combo, []).append(color(operator))
                elif matches[operator][0] == "3":
                    try:
                        # 1 and 2 stars can be avoided with longer recruitment times, but 3 stars ruin 4+ guarantees still
                        comboMatches.pop(combo)
                    except:
                        pass

guaranteeds_found = []
if len(comboMatches) > 0 and len(tags) > 1:
    truth = True
    print("\n Tags that guarantee a specific", rating, "star operator:")
    for combo in comboMatches:
        if len(comboMatches[combo]) == 1:
            # Since I skip any operators lower than the highest guarantee tag, this extra if-block ensures any guarantee combos include the guarantee tag for less confusion
            if (
                any(i in tags for i in ["Senior Operator", "Top Operator"])
                == any(i in combo for i in ["Senior Operator", "Top Operator"])
                and comboMatches[combo][0] not in guaranteeds_found
            ):
                print(
                    "With tags",
                    " + ".join(map(str, combo)),
                    "-",
                    " & ".join(map(str, comboMatches[combo])),
                )
                guaranteeds_found.append(comboMatches[combo][0])
                truth = False
    if truth:
        print("None")
    # Doesn't make sense to generally list all 4+ stars when you can already guarantee 5 or 6 stars
    if not any(i in tags for i in ["Senior Operator", "Top Operator"]):
        print("\n Tags that guarantee 4+ stars:")
        for combo in comboMatches:
            if (
                len(comboMatches[combo]) != 1
            ):
                guaranteeds_found.append(comboMatches[combo][0])
                print(
                    "With tags",
                    " + ".join(map(str, combo)),
                    "-",
                    " & ".join(map(str, comboMatches[combo])),
                )
    if args.showMode:
        print("\n Everything else:")
elif len(tags) != 1:
    print("\n No guaranteed 4+ operators \n")

if args.showMode or len(tags) == 1:
    for key in sorted(
        matches, key=lambda e: (matches[e][0], len(matches[e])), reverse=True
    ):
        if len(tags) == 1:
            print(color(key))
        else:
            print(color(key), " ".join(map(str, matches[key][1::])))
